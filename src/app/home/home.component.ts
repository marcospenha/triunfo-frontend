import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  categoriasLista;
  produtosLista;
  produtosRecentesLista;

  constructor(private homeService: HomeService) { }

  ngOnInit(): void {
    this.getCategorias();
    this.getProdutos();
    this.getProdutosRecentes();
  }

  getCategorias(){

    this.homeService.getCategorias()
      .subscribe(categoriasLista => {
        this.categoriasLista = categoriasLista;
      });

  }

  getProdutos(){

    this.homeService.getProdutos()
      .subscribe(produtosLista => {
        this.produtosLista = produtosLista;
      });

  }

  getProdutosRecentes(){

    this.homeService.getProdutosRecentes()
      .subscribe(produtosRecentesLista => {
        this.produtosRecentesLista = produtosRecentesLista;
      });

  }

}
