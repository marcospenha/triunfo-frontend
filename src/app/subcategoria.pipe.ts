import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subcategoria'
})
export class SubcategoriaPipe implements PipeTransform {

  transform(value: any[], subcategoria: number): unknown {
    //  console.log(value);

    if(subcategoria === 0){
      return value;
    }else{
      const produtosArray:any[]=[];
      for(let i = 0; i < value.length; i++){

        if(value[i].subcategorias_id === subcategoria){
          produtosArray.push(value[i]);
        }
      }
      return produtosArray;
    }

  }

}
