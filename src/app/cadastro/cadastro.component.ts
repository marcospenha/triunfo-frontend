import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CadastroService } from './cadastro.service';
import { Router } from '@angular/router';

const urlBase = 'http://triunfo.eixoposi.iuri0219.hospedagemdesites.ws/';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  public form = {
    cnpj: null,
    email: null,
    razao_social: null,
    inscricao_estadual: null,
    ramo_atividade: null,
    cep: null,
    estado: null,
    cidade: null,
    bairro: null,
    endereco: null,
    numero: null,
    complemento: null,
    ponto_referencia: null,
    quantos_funcionario: null,
    nome_completo: null,
    data_nascimento: null,
    sexo: null,
    profissao: null,
    telefone_fixo: null,
    telefone_celular: null,
    cep_entrega: null,
    estado_entrega: null,
    cidade_entrega: null,
    bairro_entrega: null,
    endereco_entrega: null,
    numero_entrega: null,
    complemento_entrega: null,
    ponto_referencia_entrega: null,
    email_verified_at: null,
    password: null,
    password_confirmar: null,
  };

  enderecoLista;
  aceiteTermo = false;
  menssagemErro = "";
  erro = false;
  test = false;

  constructor(
    private http: HttpClient,
    private cadastroService: CadastroService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  aceitarTermos(){
    this.aceiteTermo = !this.aceiteTermo;
  }

  onSubmit(){
    // console.log(this.validarCNPJ(this.form.cnpj));

    if(this.form.password !== this.form.password_confirmar){
      this.erro = true;
      this.menssagemErro = "Senhas não coincidem!";
      return;
    }

    return this.http.post(urlBase + 'usuario/criar', this.form)
    .subscribe(
      data => this.router.navigateByUrl('/entrar'),
      error => {
        this.menssagemErro = "Erro durante o cadastro, tente novamente mais tarde";
        this.erro = true;
        return;
      },
    );

  }

  getEndereco(){

    this.cadastroService.getEndereco(this.form.cep)
      .subscribe(enderecoLista => {
        this.form.estado = enderecoLista.uf;
        this.form.cidade = enderecoLista.localidade;
        this.form.bairro = enderecoLista.bairro;
        this.form.endereco = enderecoLista.logradouro;
        this.form.complemento = enderecoLista.complemento;
      });

  }

  getEnderecoEntrega(){

    this.cadastroService.getEndereco(this.form.cep_entrega)
      .subscribe(enderecoLista => {
        this.form.estado_entrega = enderecoLista.uf;
        this.form.cidade_entrega = enderecoLista.localidade;
        this.form.bairro_entrega = enderecoLista.bairro;
        this.form.endereco_entrega = enderecoLista.logradouro;
        this.form.complemento_entrega = enderecoLista.complemento;
      });

  }

  isDisabled(isFormValid){
    if(isFormValid === false || this.aceiteTermo === false){
      return true;
    }
    return false;
  }

}
