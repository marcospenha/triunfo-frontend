import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root' //faz o servico disponivel pra toda applicacao
})
export class CadastroService {

    constructor(private http: HttpClient){}

    getEndereco(cep){
        return this.http.get<EnderecoLista>('https://viacep.com.br/ws/'+cep+'/json/')
        .pipe(map(response => {
            return response;
        }));
    }

}

interface EnderecoLista {
  cep: string,
  logradouro: string,
  complemento: string,
  bairro: string,
  localidade: string,
  uf: string,
  unidade: string,
  ibge: string,
  gia: string
}

interface EnderecoListaResponse {
    enderecoLista: EnderecoLista[];
}
