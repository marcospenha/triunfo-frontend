import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { ContatoComponent } from "./contato/contato.component";
import { ProdutosComponent } from "./produtos/produtos.component";
import { CategoriasComponent } from "./categorias/categorias.component";
import { CadastroComponent } from "./cadastro/cadastro.component";
import { LoginComponent } from "./login/login.component";
import { PerfilComponent } from "./perfil/perfil.component";
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contato', component: ContatoComponent},
  {path: 'produto/:id', component: ProdutosComponent},
  {path: 'categoria/:id', component: CategoriasComponent},
  {path: 'cadastrar', component: CadastroComponent, canActivate: [BeforeLoginService]},
  {path: 'entrar', component: LoginComponent, canActivate: [BeforeLoginService]},
  {path: 'perfil', component: PerfilComponent, canActivate: [AfterLoginService]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
