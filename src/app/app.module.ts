import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContatoComponent } from './contato/contato.component';
import { ProdutosComponent } from './produtos/produtos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { CarrinhoComponent } from './carrinho/carrinho.component';
import { SubcategoriaPipe } from './subcategoria.pipe';
import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';
import { PerfilComponent } from './perfil/perfil.component';
import { RestaurarSenhaComponent } from './senha/restaurar-senha/restaurar-senha.component';
import { RestaurarRespostaComponent } from './senha/restaurar-resposta/restaurar-resposta.component';
import { FormsModule } from '@angular/forms';
import { TokenService } from './login/token.service';
import { AuthService } from './login/auth.service';
import { NgxMaskModule, IConfig } from 'ngx-mask';

// export const options: Partial | (() => Partial) = null;

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContatoComponent,
    ProdutosComponent,
    CategoriasComponent,
    CarrinhoComponent,
    SubcategoriaPipe,
    CadastroComponent,
    LoginComponent,
    PerfilComponent,
    RestaurarSenhaComponent,
    RestaurarRespostaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxMaskModule.forRoot(null)
  ],
  providers: [TokenService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
