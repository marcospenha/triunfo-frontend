import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  set(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  get() {
    return JSON.parse(localStorage.getItem('user'));
  }

  remove() {
    localStorage.removeItem('user');
  }

}
