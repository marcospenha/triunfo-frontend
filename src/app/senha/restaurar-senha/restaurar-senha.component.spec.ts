import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurarSenhaComponent } from './restaurar-senha.component';

describe('RestaurarSenhaComponent', () => {
  let component: RestaurarSenhaComponent;
  let fixture: ComponentFixture<RestaurarSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurarSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurarSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
