import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurarRespostaComponent } from './restaurar-resposta.component';

describe('RestaurarRespostaComponent', () => {
  let component: RestaurarRespostaComponent;
  let fixture: ComponentFixture<RestaurarRespostaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurarRespostaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurarRespostaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
