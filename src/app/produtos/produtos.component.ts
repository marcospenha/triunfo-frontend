import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProdutosService } from './produtos.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  produtosLista;

  constructor(
    private activatedRoute: ActivatedRoute,
    private produtosService: ProdutosService
  ) { }


  ngOnInit(): void {
    this.getProduto(this.activatedRoute.snapshot.params['id']);
  }


  getProduto(id){

    this.produtosService.getProduto(id)
      .subscribe(produtosLista => {
        this.produtosLista = produtosLista[0];
      });

  }

}
