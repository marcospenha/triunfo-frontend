import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const urlBase = 'http://triunfo.eixoposi.iuri0219.hospedagemdesites.ws/';

@Injectable({
    providedIn: 'root' //faz o servico disponivel pra toda applicacao
})
export class ProdutosService {

    constructor(private http: HttpClient){}

    // getCategorias(){
    //     return this.http.get<CategoriasListaResponse>(urlBase + 'listar/categorias')
    //     .pipe(map(response => {
    //         return response;
    //     }));
    // }

    getProduto(id){
        return this.http.get<ProdutosListaResponse>(urlBase + 'listar/produtos/' + id)
        .pipe(map(response => {
            return response;
        }));
    }


}

interface ProdutosLista {
    id: number;
    nome: string;
    preco: number;
    categorias_id: number;
    subcategorias_id: number;
    descricao: string;
    foto_1: string;
    foto_2: string;
    foto_3: string;
    foto_4: string;
    foto_5: string;
    foto_6: string;
    created_at: string;
    updated_at: string;
    codigo_produto: string;
    categoria: string;
    subcategoria: string;
}

interface ProdutosListaResponse {
    produtosLista: ProdutosLista[];
}
