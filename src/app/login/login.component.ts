import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { UsersService } from '../services/users.service';

const urlBase = 'http://triunfo.eixoposi.iuri0219.hospedagemdesites.ws/';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {
    email: null,
    password: null
  }

  public error = null;

  constructor(
    private http: HttpClient,
    private token: TokenService,
    private router: Router,
    private auth: AuthService,
    private user: UsersService
  ){}

  handleResponse(data) {
   this.token.handle(data.access_token);
   this.user.set(data.user);
   this.auth.changeAuthStatus(true);
   this.router.navigateByUrl('/');
 }

 handleError(error) {
   this.error = error.error.error;
 }

  ngOnInit(): void {
  }

  onSubmit(){
    return this.http.post(urlBase + 'api/auth/login', this.form)
    .subscribe(
      data => {
        this.handleResponse(data);
      },
      error => this.handleError(error)
    );

  }
}
