import { Component } from '@angular/core';
import { TokenService } from './login/token.service';
import { AuthService } from './login/auth.service'
import { Router } from '@angular/router';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'triunfo';
  public isLoggedIn: boolean;
  public userLoggedIn;

  constructor(
    private token: TokenService,
    private router: Router,
    private Auth: AuthService,
    private user: UsersService
  ){}

  ngOnInit(): void {
    this.Auth.authStatus.subscribe(value => this.isLoggedIn = value);
    this.userLoggedIn = this.user.get();
  }

  sair(event: MouseEvent){
      event.preventDefault();
      this.token.remove();
      this.user.remove();
      this.Auth.changeAuthStatus(false);
      this.router.navigateByUrl('/');
  }

}
