import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoriasService } from './categorias.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  categoriasLista;
  subcategoriasLista;
  produtosListas;
  subcategoriaFiltro = 0;
  categoriasListaTodas;

  constructor(
    private activatedRoute: ActivatedRoute,
    private categoriasService: CategoriasService,
    private router: Router,

  ) { }

  setValue(valor) { this.subcategoriaFiltro = valor; }

  ngOnInit(): void {
    this.getCategoria(this.activatedRoute.snapshot.params['id']);
    this.getSubcategoria(this.activatedRoute.snapshot.params['id']);
    this.getProdutosPorCategoria(this.activatedRoute.snapshot.params['id']);
    this.getTodasCategorias();
  }

  getTodasCategorias(){

    this.categoriasService.getTodasCategorias()
      .subscribe(categoriasListaTodas => {
        this.categoriasListaTodas = categoriasListaTodas;
      });

  }

  getCategoria(id){

    this.categoriasService.getCategoria(id)
      .subscribe(categoriasLista => {
        this.categoriasLista = categoriasLista[0];
      });

  }

  getSubcategoria(id){

    this.categoriasService.getSubCategoria(id)
      .subscribe(subcategoriasLista => {
        this.subcategoriasLista = subcategoriasLista;
      });

  }

  getProdutosPorCategoria(id){

    this.categoriasService.getProdutosPorCategoria(id)
      .subscribe(produtosListas => {
        this.produtosListas = produtosListas;
      });

  }

  gotoCategoria(id) {

    this.router.navigate(['/categoria', id]);
    this.getCategoria(id);
    this.getSubcategoria(id);
    this.getProdutosPorCategoria(id);
    this.getTodasCategorias();
  }

}
